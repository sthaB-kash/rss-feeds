FROM ruby:2.6.3
ENV BUNDLER_VERSION=2.3.9
RUN gem install bundler -v 2.3.9

WORKDIR /app
COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock  
RUN bundle check || bundle install

COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh

ENTRYPOINT ["entrypoint.sh"]

# ENTRYPOINT ['./entrypoints/docker-entrypoint.sh']

EXPOSE 3000

CMD ["rails", "server", "-b", "0.0.0.0"]
