# RSS Feeds

#### To run on the local machine
- clone the repo git@gitlab.com:sthaB-kash/rss-feeds.git
- `cd rss-feeds`
- `bundle install`
- `rails db:setup`
- `rails dev:cache`
- `rails server`   or `rails s`


 **For rspec**
run `rspec` or `rspec spec/requests/rss_feeds_spec.rb`

#### Using Docker and Docker-Compose
(must have docker and docker composed installed)
** run the following commands on the working directory **
- `docker-compose build`
- `docker-compose up -d`

- to stop the application run `docker-compose down`
- if you want to restart it again then run `docker-compose up` or `docker-compose up -d`
### Example

```<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:rss1="http://purl.org/rss/1.0/">
  <channel>
    <title>Rss Feeds</title>
    <link>http://127.0.0.1:3000/business.rss</link>
    <description>This is sample description</description>
    <language>en-us</language>
    <item>
      <title>Life and style</title>
      <description>5ive look back: ‘The label wanted a boyband with an edge – ours was pretty sharp’</description>
      <link>https://www.theguardian.com/lifeandstyle/2022/apr/16/boyband-5ive-look-back-so-much-so-young-end-badly</link>
      <guid>https://content.guardianapis.com/lifeandstyle/2022/apr/16/boyband-5ive-look-back-so-much-so-young-end-badly</guid>
      <pubDate>2022-04-16T11:00:04Z</pubDate>
    </item>
    <item>
      <title>Life and style</title>
      <description>Short on space? Take inspiration from Japan’s courtyard gardens | Alys Fowler</description>
      <link>https://www.theguardian.com/lifeandstyle/2022/apr/16/short-on-space-take-inspiration-japan-courtyard-gardens</link>
      <guid>https://content.guardianapis.com/lifeandstyle/2022/apr/16/short-on-space-take-inspiration-japan-courtyard-gardens</guid>
      <pubDate>2022-04-16T10:00:05Z</pubDate>
    </item>
    <item>
      <title>Life and style</title>
      <description>‘My heart is with the kids’: the former refugee inspiring young Londoners through football </description>
      <link>https://www.theguardian.com/lifeandstyle/2022/apr/16/my-heart-is-with-the-kids-the-former-refugee-inspiring-young-londoners-through-football</link>
      <guid>https://content.guardianapis.com/lifeandstyle/2022/apr/16/my-heart-is-with-the-kids-the-former-refugee-inspiring-young-londoners-through-football</guid>
      <pubDate>2022-04-16T10:00:05Z</pubDate>
    </item>
    <item>
      <title>Life and style</title>
      <description>‘When I arrived in my clothes, I felt all eyes on me’: Stefano Morelli’s best phone picture</description>
      <link>https://www.theguardian.com/lifeandstyle/2022/apr/16/when-i-arrived-in-my-clothes-i-felt-all-eyes-on-me-stefano-morellis-best-phone-picture</link>
      <guid>https://content.guardianapis.com/lifeandstyle/2022/apr/16/when-i-arrived-in-my-clothes-i-felt-all-eyes-on-me-stefano-morellis-best-phone-picture</guid>
      <pubDate>2022-04-16T09:00:04Z</pubDate>
    </item>
    <item>
      <title>Life and style</title>
      <description>Nigeria meets Willy Wonka: inside designer Yinka Ilori’s new studio</description>
      <link>https://www.theguardian.com/lifeandstyle/2022/apr/16/inside-designer-yinka-iloris-new-studio-nigeria-meets-willy-wonka</link>
      <guid>https://content.guardianapis.com/lifeandstyle/2022/apr/16/inside-designer-yinka-iloris-new-studio-nigeria-meets-willy-wonka</guid>
      <pubDate>2022-04-16T07:00:02Z</pubDate>
    </item>
    <item>
      <title>Life and style</title>
      <description>Why do pigs roll in mud, and when might you get to meet an alien? Try our kids’ quiz</description>
      <link>https://www.theguardian.com/lifeandstyle/2022/apr/16/why-do-pigs-roll-in-mud-and-when-might-you-get-to-meet-an-alien-try-our-kids-quiz</link>
      <guid>https://content.guardianapis.com/lifeandstyle/2022/apr/16/why-do-pigs-roll-in-mud-and-when-might-you-get-to-meet-an-alien-try-our-kids-quiz</guid>
      <pubDate>2022-04-16T06:00:53Z</pubDate>
    </item>
    <item>
      <title>Life and style</title>
      <description>What links Julian Assange with Dominic Cummings and Louis Wain? The Saturday quiz</description>
      <link>https://www.theguardian.com/lifeandstyle/2022/apr/16/what-links-julian-assange-with-dominic-cummings-the-saturday-quiz</link>
      <guid>https://content.guardianapis.com/lifeandstyle/2022/apr/16/what-links-julian-assange-with-dominic-cummings-the-saturday-quiz</guid>
      <pubDate>2022-04-16T06:00:52Z</pubDate>
    </item>
    <item>
      <title>Life and style</title>
      <description>#GetRishiQuick: start your wealth journey with the chancellor now! – Stephen Collins cartoon</description>
      <link>https://www.theguardian.com/lifeandstyle/ng-interactive/2022/apr/16/rishi-sunak-stephen-collins-cartoon</link>
      <guid>https://content.guardianapis.com/lifeandstyle/ng-interactive/2022/apr/16/rishi-sunak-stephen-collins-cartoon</guid>
      <pubDate>2022-04-16T05:00:51Z</pubDate>
    </item>
    <item>
      <title>Life and style</title>
      <description>Blind date: ‘I persuaded him to read his poetry aloud just as the waiter came over’</description>
      <link>https://www.theguardian.com/lifeandstyle/2022/apr/16/blind-date-artemis-sienna</link>
      <guid>https://content.guardianapis.com/lifeandstyle/2022/apr/16/blind-date-artemis-sienna</guid>
      <pubDate>2022-04-16T05:00:51Z</pubDate>
    </item>
    <item>
      <title>Life and style</title>
      <description>I’m at the DIY store – surrounded by things I don’t want or need | Tim Dowling</description>
      <link>https://www.theguardian.com/lifeandstyle/2022/apr/16/tim-dowling-im-at-the-diy-store-surrounded-by-things-i-dont-need</link>
      <guid>https://content.guardianapis.com/lifeandstyle/2022/apr/16/tim-dowling-im-at-the-diy-store-surrounded-by-things-i-dont-need</guid>
      <pubDate>2022-04-16T05:00:51Z</pubDate>
    </item>
  </channel>
</rss>
```
