# frozen_string_literal: true

# RssFeedsController
class RssFeedsController < ApplicationController
  before_action :set_url, on: %i[index]

  def index
    if valid_section_name?
      begin
        @response = Rails.cache.fetch(params[:section_name], expires_in: 10.minutes) do
          RestClient.get @url
        end
      rescue RestClient::MovedPermanently,
             RestClient::Found,
             RestClient::TemporaryRedirect => e
        @message = e.message
      rescue StandardError => e
        @message = e.message
      ensure
        handle_response
      end
    else
      @message = 'Invalid section name'
      render 'error.html', status: :bad_request
    end
  end

  private

  def set_url
    @url = "#{ENV['REMOTE_API_URL']}/#{params[:section_name].split('.')[0]}?api-key=#{ENV['API_KEY']}"
  end

  def valid_section_name?
    pattern = /^[a-z]+-?[a-z]+$/
    pattern.match?(params[:section_name].split('.')[0])
  end

  def handle_response
    if @response
      case @response.code
      when 200
        rel = JSON.parse @response.body
        @data = rel['response']['results']
        respond_to do |f|
          f.html
          f.xml { render xml: @data }
          f.rss { render layout: false }
          f.json { render json: @data }
        end
      when 404
        @message = JSON.parse response.body['response']['message']
        render 'error.html'
      else
        @message = 'Something went wrong! Remote Server Error'
        render 'error.html'
      end
    else
      render 'error.html'
    end
  end
end
