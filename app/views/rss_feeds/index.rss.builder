# frozen_string_literal: true

url = 'http://127.0.0.1:3000/business.rss'
title = 'Rss Feeds'
description = 'This is sample description'
language = 'en-us'

xml.instruct! :xml, version: '1.0'
xml.rss(version: '2.0', 'xmlns:rss1': 'http://purl.org/rss/1.0/') do
  xml.channel do
    xml.title(title)
    xml.link(url)
    xml.description(description)
    xml.language(language)

    @data.each do |datum|
      xml.item do
        xml.title(datum['sectionName'])
        xml.description(datum['webTitle'])
        xml.link(datum['webUrl'])
        xml.guid(datum['apiUrl'])
        xml.pubDate(datum['webPublicationDate'].to_s) # (:rfc822))
      end
    end
  end
end
