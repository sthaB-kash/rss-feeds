Rails.application.routes.draw do
  root 'welcome#index'
  get '/:section_name', to: 'rss_feeds#index'
end
