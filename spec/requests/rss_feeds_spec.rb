require 'rails_helper'

RSpec.describe '/:section_name', type: :request do
  let(:valid_section_name) do
    { value: 'business' }
  end

  let(:invalid_section_name) do
    { value: 'news24-' }
  end

  context 'with invalid section name' do
    describe 'Get /index' do
      it 'should return bad request' do
        get "/#{invalid_section_name[:value]}"

        expect(response).to have_http_status(:bad_request)
      end

      it 'should return bad request for \'34-gs\'' do
        section_name = '34-gs'
        get "/#{section_name}"

        expect(response).to have_http_status(:bad_request)
      end
    end
  end

  context 'with valid section name' do
    describe 'GET /index' do
      it 'should respond latest articles' do
        get "/#{valid_section_name[:value]}.rss"
        url = "#{ENV['REMOTE_API_URL']}/#{valid_section_name[:value]}?api-key=#{ENV['API_KEY']}"
        rel = RestClient.get url

        expect(rel.code).to eq(200)
      end

      it 'should respond ok' do
        get "/#{valid_section_name[:value]}.rss"

        expect(response).to have_http_status(:ok)
      end
    end
  end
  
end
